###Heat map

install.packages("usmap")
install.packages("multipanelfigure")

library(tidyverse)
library(usmap)
library(ggplot2)
library(multipanelfigure)
library(micromapST)

redesign <- read.csv("ProjectData-2014.csv",stringsAsFactors=FALSE)
colnames(redesign) <- c("State", "Population", "Travel", "Crashes", "Deaths", "DeathsIn100000", "Death_Travel")
redesign$State <- gsub("\xca", "", redesign$State)
redesign$Population <- gsub(",", "", redesign$Population)
redesign$Travel <- gsub(",", "", redesign$Travel)
redesign$Crashes <- gsub(",", "", redesign$Crashes)
redesign$Deaths <- gsub(",", "", redesign$Deaths)
redesign[ ,c(2:5)] <- lapply(redesign[ ,c(2:5)], as.numeric)

map1 <- redesign[,c(1,5)]
colnames(map1)= c("state", "deaths")
map2 <- redesign[,c(1,2)]
colnames(map2)= c("state", "population")
map3 <- redesign[,c(1,3)]
colnames(map3)= c("state", "travel")
map4 <- redesign[,c(1,4)]
colnames(map4)= c("state", "crashes")


plot1<-plot_usmap(data = map1, values = 'deaths') + 
  scale_fill_continuous(low = "white", high = "blue",
                        name = "Deaths (2014)", label = scales::comma) + 
  labs(title = "US Car Crash Causing Deaths") +
  theme(legend.position = "right", plot.title = element_text(hjust = 0.5, size = 20))

plot2<-plot_usmap(data = map2, values = 'population') + 
  scale_fill_continuous(low = "white", high = "red",
                        name = "Population (2014)", label = scales::comma) + 
  labs(title = "US Population") +
  theme(legend.position = "right", plot.title = element_text(hjust = 0.5, size = 20))


plot3<-plot_usmap(data = map3, values = 'travel') + 
  scale_fill_continuous(low = "white", high = "green",
                        name = "Travel Miles (2014)", label = scales::comma) + 
  labs(title = "US Travel Miles") +
  theme(legend.position = "right", plot.title = element_text(hjust = 0.5, size = 20))

plot4<-plot_usmap(data = map4, values = 'crashes') + 
  scale_fill_continuous(low = "white", high = "yellow",
                        name = "Car Crash (2014)", label = scales::comma) + 
  labs(title = "US Total Car Crash") +
  theme(legend.position = "right", plot.title = element_text(hjust = 0.5, size = 20))

finalplot <- multi_panel_figure(columns = 2, rows = 2, panel_label_type = "none",  width = 300, height =200,  row_spacing = c(10,10), column_spacing = c(20, 20))
finalplot %<>%
  fill_panel(plot1, column = 1, row = 1) %<>%
  fill_panel(plot2, column = 2, row = 1) %<>%
  fill_panel(plot3, column = 1, row = 2) %<>%
  fill_panel(plot4, column = 2, row = 2)

finalplot

###############################################################

#Individual Heat Map plots - with labelled states

plot1<-plot_usmap(data = map1, values = 'deaths', labels = TRUE) + 
  scale_fill_continuous(low = "white", high = "blue",
                        name = "Deaths (2014)", label = scales::comma) + 
  labs(title = "US Car Crash Causing Deaths") +
  theme(legend.position = "right", plot.title = element_text(hjust = 0.5))

plot1

plot2<-plot_usmap(data = map2, values = 'population', labels = TRUE) + 
  scale_fill_continuous(low = "white", high = "red",
                        name = "Population (2014)", label = scales::comma) + 
  labs(title = "US Population") +
  theme(legend.position = "right", plot.title = element_text(hjust = 0.5))

plot2

plot3<-plot_usmap(data = map3, values = 'travel', labels = TRUE) + 
  scale_fill_continuous(low = "white", high = "green",
                        name = "Travel Miles (2014)", label = scales::comma) + 
  labs(title = "US Travel Miles") +
  theme(legend.position = "right", plot.title = element_text(hjust = 0.5))

plot3

plot4<-plot_usmap(data = map4, values = 'crashes', labels = TRUE) + 
  scale_fill_continuous(low = "white", high = "yellow",
                        name = "Car Crash (2014)", label = scales::comma) + 
  labs(title = "US Total Car Crash") +
  theme(legend.position = "right", plot.title = element_text(hjust = 0.5, size = 20))

plot4

###############################################################
#Micromap

micromp <- read.csv("ProjectData-2014.csv", stringsAsFactors = FALSE)
View(micromp)
colnames(micromp) <- c("State", "Population", "Travel", "Crashes", "Deaths", "DeathsIn100000", "Death_Travel")
micromp$State <- gsub("","",micromp$State)
micromp$Population <- gsub(",","",micromp$Population)
micromp$Travel <- gsub(",","",micromp$Travel)
micromp$Crashes <- gsub(",","",micromp$Crashes)
micromp$Deaths <- gsub(",","",micromp$Deaths)
micromp[ ,c(2:5)] <- lapply(micromp[ ,c(2:5)], as.numeric)
paneldesc <- data.frame(
  type = c("mapmedian", "id", "dot", "bar","dot","scatdot"),
  lab2 = c('','','Deaths','Total Vehicle Miles','Fatal Crashes', 'Death VS Vehicle Miles Travelled'),
  lab3 = c('','','Deaths','Miles in Total', 'Total Crashes', 'Correlation Graph'),
  col1 = c(NA,NA,'Deaths','Travel','Crashes','Deaths'),
  col2 = c(NA,NA,'DeathsIn100000', 'Death_Travel','','Travel')
)
#windows(20, 15, xpinch = 80, ypinch = 80, point = 10)	
pdf(file="Micromap.pdf",width=9.5,height=10)
micromapST( 
  micromp, paneldesc,
  rowNamesCol = "State",
  rowNames = "full",
  plotNames = "ab",
  bordGrp = "USStatesBG",
  sortVar = "Deaths",ascend = FALSE,
  title = c("Micromap - Fatality rate from Road Car Crashes"),
  ignoreNoMatches = TRUE
)
dev.off()

###############################################################

library(tidyverse)

library(mvtsplot)

library(tidyr)

library(ggplot2)

library(scatterplot3d)

install.packages("scatterplot3d", repos = "http://cran.us.r-project.org")

install.packages("mvtsplot", repos = "http://cran.us.r-project.org")

redesign <- read.csv(file="ProjectData-2014.csv",header=T, as.is=TRUE)

#Smooth Density Curve

ggplot(redesign,aes(x=Deaths.per.100.000.population,y=Deaths.per.100.million.vehicle.miles.traveled)) +
  
  geom_point(shape=21, size=3, fill="green", color="black")+
  
  geom_smooth(method=loess, size=1.2, span=.6)+
  
  labs( x = "Deaths per 100.000 population",
        
        title="Smooth Density Curve for Deaths and Miles travelled",
        
        y= "Deaths per 100 million miles travelled")+
  
  theme(plot.title=element_text(hjust=0.5))

######################################################################################################################

#histplot

histPlot <- ggplot(redesign,aes(x=Deaths.per.100.000.population,..density..))+
  
  geom_histogram(binwidth=2, fill="pink",color="black")

histPlot+ geom_line(stat="density",color="blue",size=1.2)+
  
  labs( x= "Number of Deaths per 100.000 Population",
        
        title = "Density of the Deaths",
        
        y= "Density")+
  
  theme(plot.title=element_text(hjust=0.5))

######################################################################################################################

#3D Scatter Plot

attach(redesign)

scatterplot3d(Deaths.per.100.000.population,Deaths,Fatal.crashes, pch=16, highlight.3d=TRUE,
              
              type="h", main="3D Scatterplot for Deaths, Fatal Crashes and Deaths per 100.000 Popultion")

####################################################################################################################

#barGraph

mvtsplot(redesign[, 4:7], xtime=redesign$State, smooth.df=2, levels=9, margin=TRUE)

redesign$Deaths <- as.numeric(as.character(redesign$Deaths))

ggplot(redesign, aes(x= reorder(State,-Deaths),y=Deaths)) +
  
  geom_bar(aes(fill = State), stat = "identity", position = "dodge")+
  
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  
  labs( x = "Name of the State",
        
        title="Displaying number of Deaths for each State",
        
        y= "Number of Deaths")+
  
  theme(plot.title=element_text(hjust=0.5))
